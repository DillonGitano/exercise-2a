﻿using System;
using System.Linq;
using System.Text;


namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray()
    {
      var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)0.49, (decimal)149.99);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    public static void OutputArray(decimal[] arr)
    {
      foreach (var i in arr)
      {
        Console.WriteLine(i);
      }
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    public static decimal AverageArrayValue(decimal[] arr)
    {
      decimal average = arr.Average();

      return average;
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    public static void MaxArrayValue(decimal[] arr)
    {
      decimal max = arr.Max();
      /*throw new NotImplementedException();*/
      Console.Write("this is my max {0}", max);
    }

    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    public static void MinArrayValue(decimal[] arr)
    {
      decimal min = arr.Min();
      /*throw new NotImplementedException();*/
      Console.Write("this is my min {0}", min);
    }

    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    public static decimal[] SortArrayAsc(decimal[] arr)
    {
      Array.Sort(arr);
      return arr;
      /*throw new NotImplementedException();*/
    }

    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}