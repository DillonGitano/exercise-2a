﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      var arr = ArrayFactory.GetArray();

      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** AverageArrayValue ****");
      var average = ArrayFactory.AverageArrayValue(arr);
      Console.WriteLine("{0:C}", average);

      Console.WriteLine("\r\n**** MinArrayValue ****");
      ArrayFactory.MinArrayValue(arr);

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      ArrayFactory.MaxArrayValue(arr);

      Console.WriteLine("\r\n**** SortArrayAsc ****");
      var sort = ArrayFactory.SortArrayAsc(arr);
      ArrayFactory.OutputArray(sort);

      Console.Read();
    }
  }
}